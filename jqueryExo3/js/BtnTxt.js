/*
  Programme servant à générer du texte sur une page HTML lors d'un clic
  sur bouton grace au JQuery
  Fait le 29/03/2020 par Paul Budenhulzer 
*/
$(document).ready(function () {
    //exécuter après le chargement du DOM
    //Execution de la fonction anonyme quand nous utlisions le clic sur le bouton 
    $("#btn1").click(       
        function () {
            $("#maDiv1").html("Coucou du Jquery grace a ce bouton");   
        }
    );
});