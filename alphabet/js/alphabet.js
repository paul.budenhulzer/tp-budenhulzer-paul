//Programme servant à afficher l'alphabet en majuscule
//On utilise l'unicode de chaque lettre
//Fait le 29/03/2020 par Paul Budenhulzer

function AfficherAlphabet() {
//Fonction asssocié à la conversion de l'unicode vers une chaine de caractère
    //Déclaration des variables:
    let code = 65;      //la lettre A
    let res;            //conversion de l'unicode en caractère
    let affFinal = "";  //Affichage final

    //Conversion chaque lettre
    for (code = 65; code <= 90; code++) {       //Unicode de A à Z : 65 à 90
        res = String.fromCharCode(code);        //Convertir en charactère
        affFinal += res + " - ";                //concaténation pour affichage final
    }
    return affFinal;
}

affFinal = AfficherAlphabet();                          //Utilisation de la fonction
affFinal = affFinal.substring(0, affFinal.length - 3);  //Pour enlevé le " - " à la fin 
alert(affFinal);                                        //Affichage final