$(document).ready(function () {
    //Tout votre code Javascript ici
    $("#maDiv1").html("Coucou de jQuery");
    $("#maDiv1").css("border", "3px solid red");

    let divAffichee = false;
    $("#btn1").click(function () {

        if (divAffichee === false) {

            $("#maDiv1").show("slow");
            $("#btn1").attr("value", "Fermer");



        } else {

            $("#maDiv1").hide("slow");
            $("#btn1").attr("value", "Ouvrir");
        }
        divAffichee = !divAffichee;
    });


});