//Programme qui affiche les 17 premiers nombres de la suite de Fibonacci.
//Fait le 26/03/2020 par Paul Budenhulzer

function fibonacci() {
   
    const nbFibonacci = 17; // Constante pour le nombre de termes
    let val1 = 0, // Pour le premier terme
        val2 = 1, // Pour le second terme
        val3 = 0, // Pour les valeurs de fibonnacci
        aff = val1 + "-"; // aff final

    // Pour i allant de 2 à 17 au pas de 1 faire
    for (let i = 2; i <= nbFibonacci; i++) {

        // Pour u+1
        val3 = val1 + val2;

        // Pour u
        val2 = val1;
        val1 = val3;

        aff += val3 + "-"; // On gère l'affichage
    }
    // Pour enlever le "-" a la fin
    aff = aff.substring(0, aff.length - 1);

    return aff; // On retourne aff
}

let res = ""; //  Pour récupere l'affichage
res = fibonacci(); // res = fibonacci();
alert(res); //  On affiche res