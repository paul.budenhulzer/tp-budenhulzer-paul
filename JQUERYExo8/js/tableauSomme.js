/*  Ce programme servant à  calculer la somme des valeurs d'un tableau et l'afficher dans le pied de tableau      
    Fait par Paul Budenhulzer le 23/05/2020
    
*/

$(document).ready(function () {
    //Code à exécuter après le chargement du DOM    

    function calculTotal() {
        //Cette fonction sert à calculer la somme des numéros entrés dans un tableau 
        let total = 0;
        // variable qui contient le total de la somme

        $("#corpsTab td").each(function (index) {
            // Sélection de chacune des balises td dans le corps du tableau                                   

            total += parseInt($(this).html());
            // concaténation des anciennes valeurs + la nouvelle                              
        })
        return total;
        // retourne le total à afficher                                                       
    }

    $("#somme").html(calculTotal());
    // total des valeurs du tableau affiché dans le pied du tableau

    $("#BtnAdd").click(function () {
        //Execution de la fonction anonyme lors du clic sur le bouton "Ajouter"   

        let numeroLigne = $("tbody tr").length + 1;
        //Chaque nouvelle cellule contient le numero de la ligne

        $("tbody").append("<tr><td>" + numeroLigne + "</td></tr>");
        // nouvelle ligne + sa valeur   

        $("#somme").html(calculTotal());
        // Execution de la fonction calculTotal 

    })

    $("#BtnDel").click(function () {
        //Execution de la fonction anonyme lors du clic sur le bouton "Supprimer"

        $("#corpsTab tr:last-child").remove();
        //Sélection de la dernier balise tr dans le corps du tableau pour la supprimer

        $("#somme").html(calculTotal());
        // Execution de la fonction calculTotal 
    })

});