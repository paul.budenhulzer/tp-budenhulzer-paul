//Programme permettant de creer un tableau de Valeurs
//Fait le 05/04/2020 par Paul Budenhulzer


$(document).ready(function () {                                             
    $("#total").html(calculTotal());                                        // afficher total des valeurs
    
    
    $("#BtnAjouter").click(function () {                                    // Bouton Ajouter
        let longueurliste = $("tbody tr").length + 1;                       // variable prennantla longueur de la liste
        $("tbody").append("<tr><td>" + longueurliste + "</td></tr>");       // ajout d'une ligne + valeur
        $("#total").html(calculTotal());                                    // calcul total grâce à la fonction
    })


    $("#BtnSupprimer").click(function () {                                  // Bouton Supprimer
        $("tbody tr:last-child").remove();                                  // supprime dernier élément
        $("#total").html(calculTotal());                                    // calcul total grâce à la fonction
    })

    
    function calculTotal() {                                                // fonction pour calculer
        let somme = 0;                                                      // somme = total des valeurs
        $("tbody td").each(function (index) {                               
            somme += parseInt($(this).html());                              // ajout valeur de l'élément
        })
        return somme;                                                       // retourne la somme = la valeurs total
    }
});
